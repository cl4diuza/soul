require("@nomiclabs/hardhat-waffle");
require('@nomiclabs/hardhat-ethers');
require("@nomiclabs/hardhat-etherscan");
require('@openzeppelin/hardhat-upgrades');

// This is a sample Hardhat task. To learn how to create your own go to
// https://hardhat.org/guides/create-task.html
task("accounts", "Prints the list of accounts", async (taskArgs, hre) => {
  const accounts = await hre.ethers.getSigners();

  for (const account of accounts) {
    console.log(account.address);
  }
});

// You need to export an object to set up your config
// Go to https://hardhat.org/config/ to learn more 

/**
 * @type import('hardhat/config').HardhatUserConfig
 */
module.exports = {
  solidity: {
    compilers: [
      {
        version: "0.8.6",
        settings: {
          optimizer: {
            enabled: true
          }
        }
      },
      {
        version: "0.6.12",
        settings: {
          optimizer: {
            enabled: true
          }
        }
      }
    ]
  },
  networks: {
    bsc_testnet: {
      url: "https://data-seed-prebsc-1-s2.binance.org:8545/",
      allowUnlimitedContractSize: true,
      accounts: ["08284fdf120936db9c2077b22c2ea7462a9738f491ca7044303aca8c7920006c"]
    }
  },
  etherscan: {
    apiKey: "PPSEVHETR6WQR6Y6M7J1718S9XSX27MBHC"
  }
};
