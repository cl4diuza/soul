const { ethers, upgrades } = require('hardhat');
const { URLSearchParams } = require('url');
async function main() {
  
  global.URLSearchParams = URLSearchParams;
  
  await hre.run(
    "verify:verify", {
      address: "0xb13Dc95d211dEe516bA1D0E41e0B59Bb2B9249f2"
  }
  );
}

main()
  .then(() => process.exit(0))
  .catch(error => {
    console.error(error);
    process.exit(1);
  });
