const hre = require("hardhat");
const { URLSearchParams } = require('url');
const POOL_ADDRESS = "0x0a1AFc76b9B002B66188D11491129b1dC68aC06C";
const ORACLE_PRICE_ADDRESS = "0xe620Cb94668D9d09543A675773F1149EF033Ad6F";
const BUSD_ADDRESS = "0x78867BbEeF44f2326bF8DDd1941a4439382EF2A7";
const IDOL_ADDRESS = "0xf10362302f2a43e2cdce835db8501860fcaa69b6"

async function main() {
  // console.log('deployment start.');
  
  global.URLSearchParams = URLSearchParams;
  // // Init Contract Token
  const Subscription = await hre.ethers.getContractFactory("Subscription");

  // // Deploy Contract 
  console.log("------------- Start Deployed  ---------------");
  const subscription = await Subscription.deploy();
  console.log("subscription: ", subscription.address);
  console.log("------------- Deployed  ---------------");

  await Promise.all([
    subscription.deployed(),
  ]);

  // Verify Subscription Contract
  await hre.run(
    "verify:verify", {
      address: subscription.address
  }
  );
  console.log('Deployment done.');
}

main()
  .then(() => process.exit(0))
  .catch(error => {
    console.error(error);
    process.exit(1);
  });
