const { ethers, upgrades } = require('hardhat');

async function main () {
  const MainPoolV4 = await ethers.getContractFactory('MainPoolV4');
  console.log('Upgrading MainPoolV4...');
  await upgrades.upgradeProxy('0x0a1AFc76b9B002B66188D11491129b1dC68aC06C', MainPoolV4);
  console.log('MainPoolV4 upgraded');
}

main();