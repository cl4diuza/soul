const { ethers, upgrades } = require('hardhat');

async function main () {
  const Subscription = await ethers.getContractFactory('Subscription');
  console.log('Upgrading Subscription...');
  await upgrades.upgradeProxy('0x907402759AdBb28aCF9488A296341D99d589E5AD', Subscription);
  console.log('SubscriptionV3 upgraded');
}

main();