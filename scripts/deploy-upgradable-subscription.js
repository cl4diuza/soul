const { ethers, upgrades } = require('hardhat');

const POOL_ADDRESS = "0x0a1AFc76b9B002B66188D11491129b1dC68aC06C";
const ORACLE_PRICE_ADDRESS = "0xe620Cb94668D9d09543A675773F1149EF033Ad6F";
const BUSD_ADDRESS = "0x78867BbEeF44f2326bF8DDd1941a4439382EF2A7";
const IDOL_ADDRESS = "0xf10362302f2a43e2cdce835db8501860fcaa69b6";

async function main () {
    const Subsciption = await ethers.getContractFactory('Subscription');
    console.log('Deploying Subsciption...');
    const subsciption = await upgrades.deployProxy(
        Subsciption,
        [POOL_ADDRESS, ORACLE_PRICE_ADDRESS, BUSD_ADDRESS, IDOL_ADDRESS],
        { initializer: 'initialize' }
    );
    await subsciption.deployed();
    console.log('MainPool Subsciption to:', subsciption.address);
}

main();