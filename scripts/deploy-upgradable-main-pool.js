const { ethers, upgrades } = require('hardhat');
// address []
const IDOL_ADDRESS = "0xf10362302f2a43e2cdce835db8501860fcaa69b6";
const PEACH_ADDRESS = "0x5081F7d88Cba44e88225Cb177c41e16c1635e22A";
const BUSD_ADDRESS = "0x78867bbeef44f2326bf8ddd1941a4439382ef2a7";
const FEE_ADDRESS = "0x9be29dD2bcA0a42F738D9478295bd0e2dAff433F";
// uint []
const SUBSCRIBE_FEE = "100000000000000000";  
const SUBSCRIBE_IDOL_FEE = "60000000000000000";


async function main () {
    const ADDRESS_ARRAY = [IDOL_ADDRESS, PEACH_ADDRESS, BUSD_ADDRESS, FEE_ADDRESS];
    const BIG_INT_ARRAY = [SUBSCRIBE_FEE, SUBSCRIBE_IDOL_FEE];
    const MainPool = await ethers.getContractFactory('MainPool');
    console.log('Deploying MainPool...');
    const mainPool = await upgrades.deployProxy(MainPool, [ADDRESS_ARRAY, BIG_INT_ARRAY], { initializer: 'initialize' });
    await mainPool.deployed();
    console.log('MainPool deployed to:', mainPool.address);
}

main();