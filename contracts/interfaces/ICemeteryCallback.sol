// SPDX-License-Identifier: MIT

pragma solidity 0.8.6;

interface ICemeteryCallback {
  function cemeteryCall(
    address stakeToken,
    address userAddr,
    uint256 unboostedReward,
    uint256 lastRewardBlock
  ) external;
}