// SPDX-License-Identifier: GPL-3.0
pragma solidity 0.8.6;

import "@openzeppelin/contracts-upgradeable/token/ERC20/ERC20Upgradeable.sol";
import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/utils/math/SafeMathUpgradeable.sol";
import "../interfaces/IDracula.sol";
import "../interfaces/ISoul.sol";

contract Dracula is ERC20Upgradeable, IDracula, OwnableUpgradeable {
  using SafeMathUpgradeable for uint256;
  /// @notice soul token
  ISoul public soul;

  function initialize(ISoul _soul) external initializer {
    OwnableUpgradeable.__Ownable_init();
    ERC20Upgradeable.__ERC20_init("Dracula Token", "DRC");

    soul = _soul;
  }

  /// @dev A generic transfer function
  /// @param _to The address of the account that will be credited
  /// @param _amount The amount to be moved
  function transfer(address _to, uint256 _amount) public virtual override returns (bool) {
    _transfer(_msgSender(), _to, _amount);
    return true;
  }

  /// @dev A generic transferFrom function
  /// @param _from The address of the account that will be debited
  /// @param _to The address of the account that will be credited
  /// @param _amount The amount to be moved
  function transferFrom(
    address _from,
    address _to,
    uint256 _amount
  ) public virtual override returns (bool) {
    _transfer(_from, _to, _amount);
    _approve(
      _from,
      _msgSender(),
      allowance(_from, _msgSender()).sub(_amount, "Dracula::transferFrom::transfer amount exceeds allowance")
    );
    return true;
  }

  /// @notice Mint `_amount` DRC to `_to`. Must only be called by MasterBarista.
  /// @param _to The address to receive DRC
  /// @param _amount The amount of DRC that will be mint
  function mint(address _to, uint256 _amount) external override onlyOwner {
    _mint(_to, _amount);
  }

  /// @notice Burn `_amount` DRC to `_from`. Must only be called by MasterBarista.
  /// @param _from The address to burn DRC from
  /// @param _amount The amount of DRC that will be burned
  function burn(address _from, uint256 _amount) external override onlyOwner {
    _burn(_from, _amount);
  }

  /// @notice Safe Soul transfer function, just in case if rounding error causes pool to not have enough Souls.
  /// @param _to The address to transfer Soul to
  /// @param _amount The amount to transfer to
  function safeSoulTransfer(address _to, uint256 _amount) external override onlyOwner {
    uint256 _soulBal = soul.balanceOf(address(this));
    if (_amount > _soulBal) {
      soul.transfer(_to, _soulBal);
    } else {
      soul.transfer(_to, _amount);
    }
  }
}