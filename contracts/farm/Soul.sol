// SPDX-License-Identifier: GPL-3.0

pragma solidity 0.8.6;

import "@openzeppelin/contracts-upgradeable/token/ERC20/extensions/ERC20BurnableUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/access/AccessControlUpgradeable.sol";
import "@openzeppelin/contracts/utils/cryptography/MerkleProof.sol";
import "@openzeppelin/contracts-upgradeable/utils/math/SafeMathUpgradeable.sol";
import "../interfaces/ISoul.sol";

contract Soul is ERC20BurnableUpgradeable, OwnableUpgradeable , AccessControlUpgradeable {
  using SafeMathUpgradeable for uint256;
  // This is a packed array of booleans.
  mapping(uint256 => uint256) private claimedBitMap;

  /// @dev private state variables
  uint256 private _totalLock;
  mapping(address => uint256) private _locks;
  mapping(address => uint256) private _lastUnlockBlock;

  /// @dev public immutable state variables
  uint256 public startReleaseBlock;
  uint256 public endReleaseBlock;
  bytes32 public merkleRoot;

  /// @dev public mutable state variables
  uint256 public cap;

  // SOUL token
  ERC20Upgradeable public soul;

  bytes32 public constant GOVERNOR_ROLE = keccak256("GOVERNOR_ROLE"); // role for setting up non-sensitive data
  bytes32 public constant MINTER_ROLE = keccak256("MINTER_ROLE"); // role for minting stuff (owner + some delegated contract)
  address public constant DEAD_ADDR = 0x000000000000000000000000000000000000dEaD;

  /// @dev events
  event LogLock(address indexed to, uint256 value);
  event LogCapChanged(uint256 prevCap, uint256 newCap);
  // This event is triggered whenever a call to #claim succeeds.
  event LogClaimedLock(uint256 index, address indexed account, uint256 amount);
  event LogRedeem(address indexed account, uint256 indexed amount);
  
  function initialize(ERC20Upgradeable _soul, bytes32 _merkleRoot) public {
    OwnableUpgradeable.__Ownable_init();
    ERC20Upgradeable.__ERC20_init("Soul Token", "SOU");
    __ERC20_init_unchained("Soul Token", "SOU");
    __ERC20Burnable_init_unchained();
    _setupRole(DEFAULT_ADMIN_ROLE, _msgSender());
    _setupRole(GOVERNOR_ROLE, _msgSender());
    merkleRoot = _merkleRoot;
    startReleaseBlock = ISoul(address(_soul)).startReleaseBlock();
    endReleaseBlock = ISoul(address(_soul)).endReleaseBlock();

    require(address(_soul) != address(0), "SOUL::constructor::soul cannot be a zero address");

    soul = _soul;
  }

  modifier beforeStartReleaseBlock() {
    require(
      block.number < startReleaseBlock,
      "SOUL::beforeStartReleaseBlock:: operation can only be done before start release"
    );
    _;
  }

  /// @dev only the one having a GOVERNOR_ROLE can continue an execution
  modifier onlyGovernor() {
    require(hasRole(GOVERNOR_ROLE, _msgSender()), "SOUL::onlyGovernor::only GOVERNOR role");
    _;
  }

  /// @dev only the one having a MINTER_ROLE can continue an execution
  modifier onlyMinter() {
    require(hasRole(MINTER_ROLE, _msgSender()), "SOUL::onlyMinter::only MINTER role");
    _;
  }

  /// @dev Return unlocked SOUL
  function unlockedSupply() external view returns (uint256) {
    return totalSupply().sub(totalLock());
  }

  /// @dev Return totalLocked SOUL
  function totalLock() public view returns (uint256) {
    return _totalLock;
  }

  /// @dev Set cap. Cap must lower than previous cap. Only Governor can adjust
  /// @param _cap The new cap
  function setCap(uint256 _cap) external onlyGovernor {
    require(_cap < cap, "SOUL::setCap::_cap must < cap");
    uint256 _prevCap = cap;
    cap = _cap;
    emit LogCapChanged(_prevCap, cap);
  }

  /// @dev A function to mint SOUL. This will be called by a minter only.
  /// @param _to The address of the account to get this newly mint SOUL
  /// @param _amount The amount to be minted
  function mint(address _to, uint256 _amount) external onlyMinter {
    require(totalSupply().add(_amount) < cap, "SOUL::mint::cap exceeded");
    _mint(_to, _amount);
  }

  /// @dev A generic transfer function
  /// @param _recipient The address of the account that will be credited
  /// @param _amount The amount to be moved
  function transfer(address _recipient, uint256 _amount) public virtual override returns (bool) {
    _transfer(_msgSender(), _recipient, _amount);
    return true;
  }

  /// @dev A generic transferFrom function
  /// @param _sender The address of the account that will be debited
  /// @param _recipient The address of the account that will be credited
  /// @param _amount The amount to be moved
  function transferFrom(
    address _sender,
    address _recipient,
    uint256 _amount
  ) public virtual override returns (bool) {
    _transfer(_sender, _recipient, _amount);
    _approve(
      _sender,
      _msgSender(),
      allowance(_sender, _msgSender()).sub(_amount, "SOUL::transferFrom::transfer amount exceeds allowance")
    );
    return true;
  }

  /// @dev Return the total balance (locked + unlocked) of a given account
  /// @param _account The address that you want to know the total balance
  function totalBalanceOf(address _account) external view returns (uint256) {
    return _locks[_account].add(balanceOf(_account));
  }

  /// @dev Return the locked SOUL of a given account
  /// @param _account The address that you want to know the locked SOUL
  function lockOf(address _account) external view returns (uint256) {
    return _locks[_account];
  }

  /// @dev Return unlock for a given account
  /// @param _account The address that you want to know the last unlock block
  function lastUnlockBlock(address _account) external view returns (uint256) {
    return _lastUnlockBlock[_account];
  }

  /// @dev Lock SOUL based-on the command from MasterBarista
  /// @param _account The address that will own this locked amount
  /// @param _amount The locked amount
  function lock(address _account, uint256 _amount) external onlyMinter {
    require(_account != address(this), "SOUL::lock::no lock to token address");
    require(_account != address(0), "SOUL::lock::no lock to address(0)");
    require(_amount <= balanceOf(_account), "SOUL::lock::no lock over balance");

    _lock(_account, _amount);
  }

  /// internal function for lock, there will be NO interaction here
  function _lock(address _account, uint256 _amount) internal {
    _transfer(_account, address(this), _amount);

    _locks[_account] = _locks[_account].add(_amount);
    _totalLock = _totalLock.add(_amount);

    if (_lastUnlockBlock[_account] < startReleaseBlock) {
      _lastUnlockBlock[_account] = startReleaseBlock;
    }

    emit LogLock(_account, _amount);
  }

  /// @dev Return how many SOUL is unlocked for a given account
  /// @param _account The address that want to check canUnlockAmount
  function canUnlockAmount(address _account) public view returns (uint256) {
    // When block number less than startReleaseBlock, no Souls be unlocked
    if (block.number < startReleaseBlock) {
      return 0;
    }
    // When block number more than endReleaseBlock, all locked Souls can be unlocked
    else if (block.number >= endReleaseBlock) {
      return _locks[_account];
    }
    // When block number is more than startReleaseBlock but less than endReleaseBlock,
    // some Souls can be released
    else {
      uint256 releasedBlock = block.number.sub(_lastUnlockBlock[_account]);
      uint256 blockLeft = endReleaseBlock.sub(_lastUnlockBlock[_account]);
      return _locks[_account].mul(releasedBlock).div(blockLeft);
    }
  }

  /// @dev Claim unlocked SOUL after the release schedule is reached
  function unlock() external {
    require(_locks[msg.sender] > 0, "SOUL::unlock::no locked SOUL");

    uint256 amount = canUnlockAmount(msg.sender);

    _transfer(address(this), msg.sender, amount);
    _locks[msg.sender] = _locks[msg.sender].sub(amount);
    _lastUnlockBlock[msg.sender] = block.number;
    _totalLock = _totalLock.sub(amount);
  }

  /// @dev check whether or not the user already claimed
  function isClaimed(uint256 _index) public view returns (bool) {
    uint256 claimedWordIndex = _index / 256;
    uint256 claimedBitIndex = _index % 256;
    uint256 claimedWord = claimedBitMap[claimedWordIndex];
    uint256 mask = (1 << claimedBitIndex);
    return claimedWord & mask == mask;
  }

  /// @dev once an index (which is an account) claimed sth, set claimed
  function _setClaimed(uint256 _index) private {
    uint256 claimedWordIndex = _index / 256;
    uint256 claimedBitIndex = _index % 256;
    claimedBitMap[claimedWordIndex] = claimedBitMap[claimedWordIndex] | (1 << claimedBitIndex);
  }

  /// @notice method for letting an account to claim lock from V1
  function claimLock(
    uint256[] calldata _indexes,
    address[] calldata _accounts,
    uint256[] calldata _amounts,
    bytes32[][] calldata _merkleProofs
  ) external beforeStartReleaseBlock {
    uint256 _total = 0;
    for (uint256 i = 0; i < _accounts.length; i++) {
      if (isClaimed(_indexes[i])) continue; // if some amounts already claimed their lock, continue to another one

      // Verify the merkle proof.
      bytes32 node = keccak256(abi.encodePacked(_indexes[i], _accounts[i], _amounts[i]));
      require(MerkleProof.verify(_merkleProofs[i], merkleRoot, node), "SOUL::claimLock:: invalid proof");

      _locks[_accounts[i]] = _amounts[i];
      _lastUnlockBlock[_accounts[i]] = startReleaseBlock; // set batch is always < startReleaseBlock
      _total = _total.add(_amounts[i]);

      // Mark it claimed
      _setClaimed(_indexes[i]);
      emit LogClaimedLock(_indexes[i], _accounts[i], _amounts[i]);
    }
    _mint(address(this), _total);
    _totalLock = _totalLock.add(_total);
  }

  /// @notice used for redeem a new token from the lagacy one, noted that the legacy one will be burnt as a result of redemption
  function redeem(uint256 _amount) external beforeStartReleaseBlock {
    // burn legacy token
    soul.transferFrom(_msgSender(), DEAD_ADDR, _amount);

    // mint a new token
    _mint(_msgSender(), _amount);

    emit LogRedeem(_msgSender(), _amount);
  }
}