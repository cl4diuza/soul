## Deploy contract
Deploy price oracle
 - set router address
 - set token0 address (IDOL address)
 - set token1 address (BUSD address)
 - set band protocol standard dataset address
sss
Deploy main pol 
 - set IDOL address
 - set PEACH address 
 - set BUSD address
 - set Fee address
 - set subscribe fee
 - set IDOL subscribe fee
 - set pool type (1 idol, 2 non-idol, 3 peach-tip-pool)
 - add idol to pool type 1
 - add busd to pool type 2
 - add peach to pool type 3

Deploy subscription 
 - check oracle price contract address 
 - check main pool contract address
 - check BUSD address
 - check IDOL address

Approve token 
 - approve main pool to IDOL and PEACH token
 - approve caller to IDOL and PEACH token

Deploy subgraph
 - check subscription contract address
 - check mainPool contract address
 - check start block number


## How to upgrade contract
Init upgradable contract
- create upgradable contract
- deploy proxy with that contract (including initialize the contract)
- go to bsc and see what address proxy is pointing to
- run script and verify that contract from proxy
- verify proxy on bsc UI
- test the proxy and contract

upgrade contract
- write upgradable next version contract (no need to init again because it pick up the existing state from before)
- run script upgrade (with proxy address)
- got to bsc ui and see what proxy is pointing to now
- run script verify that contract(new v) from proxy
- verify proxy on bsc UI
- test the proxy and contract

limitation 
- upgradable can not use constructor, use initialize function instead.
- etc.

## Version
```
v9
oracle price
0xe620Cb94668D9d09543A675773F1149EF033Ad6F

main pool
0x96cC985E21f587A4FB5c5FDD90eA6B919d632D31
proxy main pool
0x0c696e9ae7921943B6ceE1EAb14dc11D500da3E2
proxy admin main pool
0xb664eDEef0F623c29C0B41cE40398652AB11b3D7

subscription
0x4f0115401a5126d6f941b175867e4e69bdc99d04
proxy subscription
0xcf3027B5C793Cd79b5fD1174f480eaddA4E46f61
proxy admin subscription
0xb664eDEef0F623c29C0B41cE40398652AB11b3D7

Subgraph
http://34.126.87.13:8000/subgraphs/name/subv9/graphql

```
